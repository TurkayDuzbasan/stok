//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Stok_Takip.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    public partial class StockSave
    {
        public int ID { get; set; }
        [Display(Name = "Urun Adi")]
        public string ProductName { get; set; }
        [Display(Name = "Urun Kodu")]
        public string ProductCode { get; set; }
        [Display(Name = "Urun Aciklama")]
        public string ProductInfo { get; set; }
        [Display(Name = "Urun Adet")]
        public string ProductNumber { get; set; }
        [Display(Name = "Urun Ucret")]
        public string ProductPayment { get; set; }
    }
}
