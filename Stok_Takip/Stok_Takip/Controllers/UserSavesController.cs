﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Stok_Takip.Models;

namespace Stok_Takip.Controllers
{
    public class UserSavesController : Controller
    {
        private StockEntities db = new StockEntities();
        public ActionResult Cıkıs()
        {
            Session["UserName"] = null;
            
            return RedirectToAction("Giris", "UserSaves");
        }
        public ActionResult Giris()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Giris(UserSave Model)
        {
            var Kullanıcı = db.UserSave.FirstOrDefault(x => x.UserName == Model.UserName && x.UserPassword == Model.UserPassword);
            var Role = db.UserSave.FirstOrDefault(x => x.Roles == Model.Roles);
            if (Kullanıcı != null)
            {
                Session["UserName"] = Kullanıcı;
                if (Kullanıcı.Roles == "Admin")
                {
                    return RedirectToAction("Index", "UserSaves");
                }

                return RedirectToAction("Index", "StockSaves");

            }
            ModelState.AddModelError("", "Lütfen Bilgilerinizi Kontrol Ediniz...");
            return View();
        }
        // GET: UserSaves
        public ActionResult Index()
        {
            return View(db.UserSave.ToList());
        }

        // GET: UserSaves/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserSave userSave = db.UserSave.Find(id);
            if (userSave == null)
            {
                return HttpNotFound();
            }
            return View(userSave);
        }

        // GET: UserSaves/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: UserSaves/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,UserName,UserPassword,UserEmail,SecurityQue,SecurityAns,Roles")] UserSave userSave)
        {
            if (ModelState.IsValid)
            {
                db.UserSave.Add(userSave);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(userSave);
        }

        // GET: UserSaves/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserSave userSave = db.UserSave.Find(id);
            if (userSave == null)
            {
                return HttpNotFound();
            }
            return View(userSave);
        }

        // POST: UserSaves/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,UserName,UserPassword,UserEmail,SecurityQue,SecurityAns,Roles")] UserSave userSave)
        {
            if (ModelState.IsValid)
            {
                db.Entry(userSave).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(userSave);
        }

        // GET: UserSaves/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserSave userSave = db.UserSave.Find(id);
            if (userSave == null)
            {
                return HttpNotFound();
            }
            return View(userSave);
        }

        // POST: UserSaves/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            UserSave userSave = db.UserSave.Find(id);
            db.UserSave.Remove(userSave);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
