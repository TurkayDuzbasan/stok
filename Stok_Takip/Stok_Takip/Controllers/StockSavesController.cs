﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Stok_Takip.Models;

namespace Stok_Takip.Controllers
{
    public class StockSavesController : Controller
    {
        private StockEntities db = new StockEntities();

        // GET: StockSaves
        public ActionResult Index()
        {
            return View(db.StockSave.ToList());
        }

        // GET: StockSaves/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StockSave stockSave = db.StockSave.Find(id);
            if (stockSave == null)
            {
                return HttpNotFound();
            }
            return View(stockSave);
        }

        // GET: StockSaves/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: StockSaves/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,ProductName,ProductCode,ProductInfo,ProductNumber,ProductPayment")] StockSave stockSave)
        {
            if (ModelState.IsValid)
            {
                db.StockSave.Add(stockSave);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(stockSave);
        }

        // GET: StockSaves/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StockSave stockSave = db.StockSave.Find(id);
            if (stockSave == null)
            {
                return HttpNotFound();
            }
            return View(stockSave);
        }

        // POST: StockSaves/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,ProductName,ProductCode,ProductInfo,ProductNumber,ProductPayment")] StockSave stockSave)
        {
            if (ModelState.IsValid)
            {
                db.Entry(stockSave).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(stockSave);
        }

        // GET: StockSaves/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            StockSave stockSave = db.StockSave.Find(id);
            if (stockSave == null)
            {
                return HttpNotFound();
            }
            return View(stockSave);
        }

        // POST: StockSaves/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            StockSave stockSave = db.StockSave.Find(id);
            db.StockSave.Remove(stockSave);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
